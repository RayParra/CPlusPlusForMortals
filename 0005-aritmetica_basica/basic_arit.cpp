/*
  basic_arit.cpp
  programa para explicar el uso de variables numericas,
  utilizaremos entradas y salidas de variavles
*/

#include<iostream>
using namespace std;

int main(int argc, char const *argv[]) {
  int x, y;
  std::cout << "Escribe un numero: " << '\n';
  std::cin >> x;
  std::cout << "Escribe otro numero: " << '\n';
  std::cin >> y;
  std::cout << "La suma de los 2 numeros es: "<< x + y << '\n';
  std::cout << "La resta de los 2 numeros es: "<< x - y << '\n';
  std::cout << "La Multiplicacion de los 2 numeros es: "<< x * y << '\n';
  std::cout << "La Division de los 2 numeros es: "<< x / y << '\n';
  return 0;
}
