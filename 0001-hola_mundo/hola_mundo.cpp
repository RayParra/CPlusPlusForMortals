// hola_mundo.cpp
// Programa para ilustrar la forma de dar salida
// a objetos en pantalla

#include<iostream>
using namespace std;

int main(int argc, char const *argv[]) {
  /* code */
  cout << "Hola Mundo XD" << endl;
  return 0;
}
