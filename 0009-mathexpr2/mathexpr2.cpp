/*
  mathexpr2.cpp
  programa para realizar el calculo de la expresion a + b / c + d
*/

#include<iostream>
using namespace std;


int main(int argc, char const *argv[]) {

  float a, b, c, d, response = 0;

  std::cout << "Escribe el valor de a " << '\n'; std::cin >> a;
  std::cout << "Escribe el valor de b " << '\n'; std::cin >> b;
  std::cout << "Escribe el valor de c " << '\n'; std::cin >> c;
  std::cout << "Escribe el valor de d " << '\n'; std::cin >> d;

  response = (a + b) /(c + d);
  cout.precision(2);
  std::cout << "El resultado de la Expresion a+b / c+d es: " << response << '\n';

  return 0;
}
