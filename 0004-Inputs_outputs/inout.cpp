/*
    inout.cpp

    programa para explciar las entradas por teclado y 
    salidas por pantalla utilizando los objetos
    cin>> y cout<<
*/

#include<iostream>
using namespace std;

int main(){
    int x, y;

    cout<<"Escribe un numero: " << endl;
    cin>> x;
    cout<<"Escribe otro numero: "<< endl;
    cin>> y;

    cout<<"Los numeros escritos son: " << x << " y : "<< y << endl;

    return 0;
}