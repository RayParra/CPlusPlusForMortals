/*
programa para resolver la siguiente expresion matematica
(a + (b/c))/(d + (e/f))
*/

#include<iostream>
using namespace std;

int main(int argc, char const *argv[]) {
  /* code */
  float a, b, c, d, e, f, response = 0;
  std::cout << "Digita el valor del parametro a: " << '\n';std::cin >> a;
  std::cout << "Digita el valor del parametro b: " << '\n';std::cin >> b;
  std::cout << "Digita el valor del parametro c: " << '\n';std::cin >> c;
  std::cout << "Digita el valor del parametro d: " << '\n';std::cin >> d;
  std::cout << "Digita el valor del parametro e: " << '\n';std::cin >> e;
  std::cout << "Digita el valor del parametro f: " << '\n';std::cin >> f;

  response = (a + (b/c))/(b + (e/f));

  std::cout << "El resultado de la expresion matematica es: " << response << '\n';
  
  return 0;
}
