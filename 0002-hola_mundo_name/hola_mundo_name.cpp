/*
  hola_mundo_name.cpp
  programa para ilustrar el uso de cout, objeto
  para dar salida a otros objetos en pantalla
  En este programa se le agrega el parametro del nombre
*/

#include<iostream>
using namespace std;

int main(int argc, char const *argv[]) {
  std::string name = "Ray Parra";
  //cin >> name;
  cout << "Hola Mundo XD, bienvenido: " << name << '\n';
  return 0;
}
