/*
  entrada_standar.cpp
  programa para explicar como funciona la entrada estandar por teclado
  utilizando el objeto cin>>
*/

#include<iostream>
using namespace std;


int main(int argc, char const *argv[]) {
  int edad;
  char sexo;
  float estatura;

  std::cout << "Escribe tu edad: " << '\n'; cin>>edad;
  std::cout << "Escribe tu Sexo(F-femenino o M-masculino): " << '\n'; cin>>sexo;
  std::cout << "Escribe tu estatura(en centimetros): " << '\n'; cin>>estatura;

  std::cout << "Tus Datos son: " << '\n';
  std::cout << "Edad: "<< edad << '\n';
  std::cout << "Sexo: "<< sexo << '\n';
  std::cout << "Estatura: "<< estatura<< " en Cm." << '\n';
  return 0;
}
