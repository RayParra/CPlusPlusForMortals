/*
  mathexpr1.cpp
  programa para realizar el calculo de la expresion a/b + 1
*/

#include<iostream>
using namespace std;


int main(int argc, char const *argv[]) {

  float a, b, response = 0;

  std::cout << "Escribe el valor de a " << '\n'; std::cin >> a;
  std::cout << "Escribe el valor de b " << '\n'; std::cin >> b;
  response = (a/b) + 1;
  std::cout << "El resultado de la Expresion a/b + 1 es: " << response << '\n';

  return 0;
}
