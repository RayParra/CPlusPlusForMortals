/*
  calcular_iva.cpp
  programa para calcular el iva(impuesto al valor agregado) de los productos
  recordar que el iva esta basado en un porcentaje que se agrega al valor del
  producto o servicio que esta en la transaccion de compra-venta. Para este
  programa utilizaremos el 11%
*/

#include<iostream>
using namespace std;

int main(int argc, char const *argv[]) {
  float iva, venta, total;
  iva = 1.11;
  std::cout << "Escriba la cantidad de la venta: " << '\n'; std::cin >> venta;
  total = venta * iva;
  std::cout << "El Total de la venta mas iva es: "<< total << '\n'; 
  return 0;
}
